unit_chain=["zéro", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf"]
chain_ten=["dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix-sept", "dix-huit", "dix-neuf"]
chain_dizaine=["", "dix", "vingt", "trente", "quarante", "cinquante", "soixante", "soixante", "quatre-vingt", "quatre-vingt"]

def conversion(number):
    get_out=''
    reste=number
    i=1000000000
    while i>0:
        y=int(reste/i)
        if y!=0:
            hundred=int(y/100)
            ten=int((y - hundred*100)/10)
            unite=int(y-hundred*100-ten*10)
            if hundred==1:
                get_out+=" cent "
            elif hundred!=0:
                get_out+= unit_chain[int(hundred)] + " cent "
                if ten==0 and unite==0:
                    get_out=get_out[:-1]+"s "
            if ten not in [0,1]:
                get_out+=chain_dizaine[ten]
            if unite==0:
                if ten in [1,7,9]:
                    get_out+="-dix "
                elif ten==8:
                    get_out=get_out[:-1]+"s "
            elif unite==1:
                if ten in [1,9]:
                    get_out+="-onze "
                elif ten==7:
                    get_out+=" et onze "
                elif ten in [2,3,4,5,6]:
                    get_out+=" et un "
                elif ten in [0,8]:
                    get_out+="-un "
            elif unite in [2,3,4,5,6,7,8,9]:
                if ten in [1,7,9]:
                    get_out += "-"
                    get_out+=chain_ten[unite]
                else:
                    get_out += "-"
                    get_out+=unit_chain[unite]
            if i==1000000000:
                if y>1:
                    get_out+=" milliards "
                else:
                    get_out+=" milliards "
            if i==1000000:
                if y>1:
                    get_out+=" millions "
                else:
                    get_out+=" millions "
            if i==1000:
                get_out+=" mille "
        reste -= y*i
        dix=False
        i/=1000;
    if len(get_out)==0:
        get_out+=unit_chain[0]
    return get_out

if __name__ == '__main__':
    print("Entrer le chiffre ou nombre que vous voulez écrire:")
    test = int(input())
    print(conversion(test))
