class Pizza:

    def __init__(self, size, ingredients):
        self.size = size
        if ingredients == []:
            raise ValueError("la liste est vide")
        else:
            self.ingredients = ingredients

        for i in ingredients :
            if type(i) != str :
                raise TypeError("Ce n'est pas un caractère")
        self.pricepizza = self.size ** 2 / 10 + 3 * len(self.ingredients)


    def discount(self,reduct):
        self.reduct = reduct
        self.pricepizza = self.pricepizza - (self.pricepizza*(self.reduct*0.01))

    def price(self):
        return self.pricepizza

giuliaPizza = Pizza (8 , [ 'mozzarella', 'gorgonzola', 'pecorino', 'parmigiano'] )
marcoPizza = Pizza(size=10, ingredients = [ 'pomodoro', 'prosciutto', 'mozzarella'] )
print(giuliaPizza.price() + marcoPizza.price())
giuliaPizza.discount(20) # rabais de 20%
marcoPizza.discount (20) # rabais de 20%
print(giuliaPizza.price() + marcoPizza.price())